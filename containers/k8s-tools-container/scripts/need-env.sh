#!/bin/sh

set -e

if set | grep -oE '^([^=]+)' | grep -Fwq "$1"; then
  true
else
  echo "Required env variable '$1' is unset" > /dev/stderr
  exit 1
fi

# Recall if needed
if [ $# -gt 0 ]; then
  shift
  exec "$0" "$@"
fi
